﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	boolean stop = false;
    	double zuZahlenderBetrag;
    	double rückgabebetrag;
    	
    	 
    	do {
    	zuZahlenderBetrag=fahrkartenbestellungErfassen();
    	rückgabebetrag=fahrkartenBezahlen(zuZahlenderBetrag);
    	fahrkartenAusgeben();
    	rueckgeldAusgeben(rückgabebetrag);
    	} while(!stop);
    	
    	
    }
    
    static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag;
    	double einzelpreis;
    	String[] tickets= {"Einzelfahrschein Berlin AB","Einzelfahrschein Berlin BC","Einzelfahrschein Berlin ABC",
    						"Kurzstrecke","Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC",
    						"Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC"};
    	int anzahl;
    	int wahl;
    	double[] preise = {2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
    	
    	do {
    	System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin aus: \n");
    	System.out.printf("%10s %30s %45s \n", "Auswahlnummer","Bezeichnung","Preise in Euro");
    	System.out.println("------------------------------------------------------------------------------------------");
    	for(int i=0;i<tickets.length;i++) {
    		System.out.printf("%-32d %-42s %-20.2f \n",i+1,tickets[i],preise[i]);
    		//System.out.println(tickets[i]);		//Auflistung der möglichen Tickets
    	}
    	
    	wahl = tastatur.nextInt();
    	if(wahl>tickets.length || wahl < 1)
    	System.out.println("Ungültige Eingabe!");
    	}while(wahl>tickets.length || wahl < 1);
    	
    	wahl--;
    	System.out.println("Ihre Wahl: " + tickets[wahl]);
    	System.out.println();
    	
    	einzelpreis=preise[wahl];
    	
        System.out.print("Anzahl der Tickets: ");
        anzahl = tastatur.nextInt();	
        while(anzahl>10||anzahl<1) { //Bei ungültigem Wert -> Neuen Wert anfordern 
        	
        	System.out.println("Ungültige Anzahl! Bitte geben Sie eine Zahl zwischen 1 und 10 ein!");
        	anzahl = tastatur.nextInt();
        }
        
        zuZahlenderBetrag=einzelpreis * anzahl; 		//Preis pro Ticket multipliziert mit der Anzahl der Tickets
        
    	
    	return zuZahlenderBetrag;
    }
    
    static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	double eingeworfeneMünze;
    	double rückgabebetrag;
    	double eingezahlterGesamtbetrag;
    	eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n",(zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag; //Rückgabebetrag wird berechnet
        return rückgabebetrag;
    }
    
    static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    static void rueckgeldAusgeben(double rueckgabebetrag) {
    	if(rueckgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f %s \n",rueckgabebetrag,"EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.195) // 20 CENT-Münzen Rundungsfehler deswegen 0.195 statt 0.2
            {
         	  System.out.println("20 CENT");
  	          rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.095) // 10 CENT-Münzen Rundungsfehler deswegen 0.095 statt 0.1
            {
         	  System.out.println("10 CENT");
 	          rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.045)// 5 CENT-Münzen Rundungsfehler deswegen 0.045 statt 0.05
            {
         	  System.out.println("5 CENT");
  	          rueckgabebetrag -= 0.05;
            }
        }
    	System.out.println();
    }
}
